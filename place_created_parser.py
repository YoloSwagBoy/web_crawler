import os
import json

path = 'output/standard/'

dummy_dict = {}


def parse_object_type(string):
    dummy_list = string.split(" / ")
    return dummy_list


def handle_all_place_created():
    for filename in os.listdir(path):
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)
            place_created = data["place_created"].replace(" (?)", "").replace(" (perhaps)", "").replace("perhaps ", "").replace("?", "")

            if place_created not in dummy_dict:
                dummy_dict[place_created] = 1
            else:
                dummy_dict[place_created] += 1

            #if "Greece" in data["place_created"]:
            #    print(data["place_created"] + (100 - len(data["place_created"]))*" " + filename)

    for x in sorted(dummy_dict):
        if "Greece" in x:
            print(x + (100 - len(x))*" " + str(dummy_dict[x]))


if __name__ == "__main__":
    handle_all_place_created()
