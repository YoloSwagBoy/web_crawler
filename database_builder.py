from tinydb import Query, TinyDB
import os
import json
from date_parser import parse_date_string
from culture_parser import parse_culture_string
from object_type_parser import parse_object_type_string
from maker_parser import parse_maker_string

'''earl = Query()
t = db.search(earl.date[0] == -500)
f = db.search(earl.date[1] == -445)
print(t)
print(f)

r = db.search(earl.culture.any("Attic"))
print(r)'''

path = 'output/standard/'


def build_data_base():
    if not os.path.exists('database'):
        os.makedirs('database')

    db = TinyDB('database/db.json')
    db.purge()

    i = 0
    for filename in os.listdir(path):
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)
            id = int(filename.split(".")[0])
            title = data["title"]
            date = parse_date_string(data["date"])
            culture = parse_culture_string(data["culture"])
            object_type = parse_object_type_string(data["object_type"])
            place_created = data["place_created"]
            medium = data["medium"]
            dimensions = data["dimensions"]
            maker = parse_maker_string(data["maker"])
            page_link = data["page"]
            db.insert({"id": id, "title": title, "date": date, "culture": culture, "object_type": object_type,
                       "place_created": place_created, "medium": medium, "dimensions": dimensions,
                       "maker": maker, "page": page_link})
            i += 1
            print(i)


if __name__ == "__main__":
    build_data_base()
