import os
import json
import re

path = 'output/standard/'
culture_list = {'Greek': 0}

pattern_greek = re.compile(r'Greek$')


def handle_greek(culture_string):
    culture_list[culture_string] += 1
    return [culture_string]


pattern_greek_plus_simple = re.compile(r'Greek +\([A-Z][a-z]*( +or +[A-Z][a-z]*)?\)( +or +[A-Z][a-z]*)?$')


def handle_greek_plus_simple(culture_string):
    culture_string = culture_string.replace("(", "").replace(")", "").replace(" or", "")
    cultures = culture_string.split(" ")

    for cs in cultures:
        if cs not in culture_list:
            culture_list[cs] = 1
        else:
            culture_list[cs] += 1

    return cultures


pattern_greek_east = re.compile(r'East +Greek$')


def handle_greek_east(culture_string):
    cpy = culture_string
    culture_string = culture_string.split(" ")

    cultures = [culture_string[1], cpy]

    for cs in cultures:
        if cs not in culture_list:
            culture_list[cs] = 1
        else:
            culture_list[cs] += 1

    return cultures


pattern_greek_east_plus = re.compile(r'East +Greek +\([A-Z][a-z]*\)$')


def handle_greek_east_plus(culture_string):
    # ignore parenthesis -> too few samples
    culture_string = culture_string.split(" ")[:-1]

    east = culture_string[0] + " " + culture_string[1]
    cultures = [culture_string[1], east]

    for cs in cultures:
        if cs not in culture_list:
            culture_list[cs] = 1
        else:
            culture_list[cs] += 1

    return cultures


pattern_greek_south_italian = re.compile(r'Greek +(\(South +Italian\)|or +South +Italian)$')
pattern_greek_south_italian_plus = re.compile(r'Greek +\(South +Italian, +[A-Z][a-z]*\)$')


def handle_greek_south_italian_simple(culture_string):
    culture_string = culture_string.replace("or ", "").replace(",", "").replace("(", "").replace(")", "")
    culture_string = culture_string.split(" ")

    cultures = [culture_string[0], culture_string[1] + " " + culture_string[2]]
    if len(culture_string) == 4:
        cultures += [cultures[-1] + ", " + culture_string[-1]]

    for cs in cultures:
        if cs not in culture_list:
            culture_list[cs] = 1
        else:
            culture_list[cs] += 1

    return cultures


pattern_greek_south_italian_plus_plus = \
    re.compile(r'Greek +\(South +Italian(, +[A-Z][a-z]*( +or +[A-Z][a-z]*)?| +or +[A-Z][a-z]*)\)$')


def handle_greek_south_italian_extended(culture_string):
    cultures = culture_string.replace(")", "").split(" (")
    if "," in cultures[-1]:
        subculture = cultures[-1].split(", ")
        subsubcultures = subculture[-1].split(" or ")
        subsubcultures = [subculture[0] + ", " + subsubcultures[0],
                          subculture[0] + ", " + subsubcultures[-1]]
        cultures = [cultures[0]] + subsubcultures
    else:
        subcultures = cultures[-1].split(" or ")
        cultures = [cultures[0]] + subcultures

    for cs in cultures:
        if cs not in culture_list:
            culture_list[cs] = 1
        else:
            culture_list[cs] += 1

    return cultures


pattern_greek_or_any = re.compile(r'Greek +or +[A-Z][a-z]*$')


def handle_greek_or_any(culture_string):
    culture_string = culture_string.replace("or ", "")
    cultures = culture_string.split(" ")

    for cs in cultures:
        if cs not in culture_list:
            culture_list[cs] = 1
        else:
            culture_list[cs] += 1

    return cultures


pattern_greek_special = re.compile(r'[A-Z][a-z]* +or +(Greek +\([A-Z][a-z]*\)|[A-Z][a-z]*, +and +Greek)$')


def handle_greek_special(culture_string):
    cultures = culture_string.replace("(", "").replace(")", "").replace(", and", "").replace(" or", "")
    cultures = cultures.split(" ")

    for cs in cultures:
        if cs in culture_list:
            culture_list[cs] += 1
        else:
            cultures.remove(cs)  # remove since very few cases

    return cultures


def print_entries(key):

    for filename in os.listdir(path):
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)
            culture = data[key]
            culture = culture.replace(" ?", "")
            culture = culture.replace("?", "")

            match = False
            subculture_list = []
            if pattern_greek.match(culture):
                match = True
                subculture_list = handle_greek(culture)
            elif pattern_greek_east.match(culture):
                match = True
                subculture_list = handle_greek_east(culture)
            elif pattern_greek_plus_simple.match(culture):
                match = True
                subculture_list = handle_greek_plus_simple(culture)
            elif pattern_greek_east_plus.match(culture):
                match = True
                subculture_list = handle_greek_east_plus(culture)
            elif pattern_greek_south_italian.match(culture) or pattern_greek_south_italian_plus.match(culture):
                match = True
                subculture_list = handle_greek_south_italian_simple(culture)
            elif pattern_greek_or_any.match(culture):
                match = True
                subculture_list = handle_greek_or_any(culture)
            elif pattern_greek_south_italian_plus_plus.match(culture):
                match = True
                subculture_list = handle_greek_south_italian_extended(culture)
            elif pattern_greek_special.match(culture):
                match = True
                subculture_list = handle_greek_special(culture)

            # replace things like "Tarantine" with "South Italian, Tarantine" since more common
            for sc in subculture_list:
                for c in culture_list:
                    if (", " + sc) in c and sc != c:
                        culture_list[sc] -= 1
                        culture_list[c] += 1
                        subculture_list.remove(sc)
                        subculture_list.append(c)

            if not match:
                # for debugging: if executed -> some file was not parsed correctly
                print()
                print("unfinished")
                print(culture)

    culture_list_clean = dict((k, v) for k, v in culture_list.items() if v != 0)  # remove keys with value 0
    for f in culture_list_clean:
        print(f + (30 - len(f))*" " + str(culture_list_clean[f]))


def parse_culture_string(culture_string):
    culture = culture_string.replace(" ?", "").replace("?", "")

    subculture_list = []
    if pattern_greek.match(culture):
        subculture_list = handle_greek(culture)
    elif pattern_greek_east.match(culture):
        subculture_list = handle_greek_east(culture)
    elif pattern_greek_plus_simple.match(culture):
        subculture_list = handle_greek_plus_simple(culture)
    elif pattern_greek_east_plus.match(culture):
        subculture_list = handle_greek_east_plus(culture)
    elif pattern_greek_south_italian.match(culture) or pattern_greek_south_italian_plus.match(culture):
        subculture_list = handle_greek_south_italian_simple(culture)
    elif pattern_greek_or_any.match(culture):
        subculture_list = handle_greek_or_any(culture)
    elif pattern_greek_south_italian_plus_plus.match(culture):
        subculture_list = handle_greek_south_italian_extended(culture)
    elif pattern_greek_special.match(culture):
        subculture_list = handle_greek_special(culture)

    return subculture_list

# TODO: write function to replace things like "Tarantine" with "South Italian, Tarantine" since more common,
# TODO: see print_entries function -> should be executed after database is built


if __name__ == '__main__':
    # parse all files in library
    print_entries('culture')