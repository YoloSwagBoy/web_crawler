import urllib.request
import urllib.error
import os
import json
import time
import threading

res = []

##couldn download
#[('http://www.getty.edu/museum/media/images/web/enlarge/13343001.jpg', '125413'),
# ('http://www.getty.edu/museum/media/images/web/enlarge/13343001.jpg', '12970'),
# ('http://www.getty.edu/museum/media/images/web/enlarge/13343001.jpg', '8017'),
# ('http://www.getty.edu/museum/media/images/web/enlarge/13343001.jpg', '8018'),
# ('http://bolt.getty.edu/collection/Museum/private/images/web/enlarge/01153701.jpg', '9824')]



def get_urls_and_names(links):
    path = 'output/standard/'
    file_counter = 0
    for filename in os.listdir(path):
        file_counter += 1
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)
            imagelink = data['image']
            #print(date)
            #print(filename)
            id = filename.split('.')[0]
            links[id] = imagelink

def download_pictures(links):
    counter = 0
    failedList = []
    for key in links:
        counter += 1
        name = key
        link = links[key]
        #print("Downloading from "+link+" with id: "+name+" NR: "+str(counter))

        try:
            urllib.request.urlretrieve(""+link+"", "images/"+name+".jpg")
        except ValueError:
            print("Error Downloading link")
            failedList.append((link, name))
        except urllib.error.HTTPError:
            print("Error Downloading link")
            failedList.append((link, name))
        except urllib.error.URLError:
            print("Error Downloading link")
            failedList.append((link, name))

        print(counter)
    print("Failed to download " + str(len(failedList)) + " images")
    print(failedList)

def run_image_downloader():
    links ={}
    get_urls_and_names(links)
    download_pictures(links)

if __name__ == "__main__":
    run_image_downloader()