from PyQt5 import QtGui, QtCore, QtWidgets, uic
import sys
import cv2

import tkinter as tk
from tkinter import filedialog


class Ui(QtWidgets.QWidget):
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('test.ui', self)
        self.show()

        self.Upload_button = self.findChild(QtWidgets.QPushButton, 'Upload')  # Find the button
        self.Upload_button.clicked.connect(self.uploadFile)
        self.image_viewer = self.findChild(QtWidgets.QLabel, 'ImageViewer')

    def uploadFile(self):
        root = tk.Tk()
        root.withdraw()

        file_path = filedialog.askopenfilename()
        print(file_path)

        img = QtGui.QImage(file_path)
        pixmap = QtGui.QPixmap(img)
        pixmap2 = pixmap.scaled(256, 256, QtCore.Qt.KeepAspectRatio)
        self.image_viewer.setPixmap(pixmap2)


        '''self.image = cv2.imread(file_path)
        self.image = QtGui.QImage(self.image.data, self.image.shape[1], self.image.shape[0],
                                  QtGui.QImage.Format_RGB888).rgbSwapped()
        self.image_viewer.setPixmap(QtGui.QPixmap.fromImage(self.image))'''

        return file_path


app = QtWidgets.QApplication(sys.argv)
window = Ui()
app.exec_()