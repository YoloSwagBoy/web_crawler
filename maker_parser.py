import util
import os
import json

path = 'output/standard/'
test_dict = {}


def parse_maker(maker_string):
    maker_list = maker_string.split(util.UNIQUE_SEPERATOR)
    assert isinstance(maker_list, list)
    return maker_list


def handle_all_makers():
    for filename in os.listdir(path):
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)
            if data['maker'] not in test_dict:
                test_dict[data['maker']] = 1
            else:
                test_dict[data['maker']] += 1

            maker_string = data['maker'].replace(u"\u2013", "-")

            print(filename + (20-len(filename))*" " + maker_string)
            maker_string_list = parse_maker(maker_string)
            print(maker_string_list)


def parse_maker_string(maker_string):
    return parse_maker(maker_string)


if __name__ == "__main__":
    handle_all_makers()
