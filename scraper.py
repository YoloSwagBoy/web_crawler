# website to be scraped: http://www.getty.edu/art/collection/

import util
import os
import json
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.crawler import CrawlerProcess
import scrapy.settings.default_settings as SETTINGS
SETTINGS.LOG_LEVEL = 'INFO'

SETTINGS.COOKIES_ENABLED = False
# SETTINGS.ROBOTSTXT_OBEY = True
SETTINGS.USER_AGENT = 'university_web_crawler_project (mb.tugraz.student@gmail.com)'

SETTINGS.CONCURRENT_REQUESTS = 32
SETTINGS.CONCURRENT_REQUESTS_PER_DOMAIN = 32
SETTINGS.DOWNLOAD_FAIL_ON_DATALOSS = False

SETTINGS.AUTOTHROTTLE_ENABLED = False
SETTINGS.DOWNLOAD_TIMEOUT = 600
SETTINGS.RETRY_TIMES = 100
SETTINGS.DOWNLOAD_DELAY = 0.25

# option == 1 extended query
# option == 2 standard query

page_url = 'http://www.getty.edu/art/collection/search/?pg='
base_url = 'http://www.getty.edu/art/collection/objects/'

output_path = 'output/'
standard_path = 'standard/'
extended_path = 'extended/'
image_exists_path = "image_exists/"
no_image_path = "no_image/"

standard_query_url =\
        'http://www.getty.edu/art/collection/search/?pg=1&query=YTo2OntzOjg6Il9jdWx0dXJlIjtzOjU6\
        IkdyZWVrIjtzOjIxOiIoY3VsdHVyZXxwbGFjZS5uYW1lcykiO3M6NToiR3JlZWsiO3M6MTM6ImRlcGFydG1lbnQuaWQiO2E6MT\
        p7aTowO2k6Mzt9czoxNzoiX2ZpbHRlcl9oYXNfaW1hZ2UiO2I6MTtzOjk6Imhhc19pbWFnZSI7YjoxO3M6NDoic29ydCI7czo2\
        OiItc2NvcmUiO30%3D'

extended_query_url =\
        'http://www.getty.edu/art/collection/search/?pg=1&query=YToxOntzOjQ6InNvcnQiO3M6NjoiLXNjb3JlIjt9'


def debug(infos, key_single=None):
    if key_single is not None:
        print(key_single + (30 - len(key_single))*" " + infos[key_single])
    else:
        for key, value in infos.items():
            print(key, "\t:", value)


def print_page_number(number):
    print('page: ', number, ' parsed!')


def dump_json(infos, option):
    # in case an art object with a certain id does not exist, no json file will be dumped
    if len(infos['object_number']) > 0:
        object_id = infos['page'].split(base_url)[-1].split('/')[0]
        final_path = ''
        if option == 1:
            # extended
            image_path = image_exists_path if len(infos['image']) > 0 else no_image_path
            final_path = output_path + extended_path + image_path
        elif option == 2:
            # standard
            final_path = output_path + standard_path
        if not os.path.exists(final_path):
            os.makedirs(final_path)
        with open(final_path + str(object_id) + '.json', 'w', encoding='utf8') as fp:
            json.dump(infos, fp, indent=2, ensure_ascii=False)


class ArtSpider(CrawlSpider):
    name = 'art scraper'
    allowed_domains = [
        'www.getty.edu'
    ]

    def __init__(self, option):
        self.option = option

        def set_start_urls(query_option):
            if query_option == 1:
                self.start_urls = [extended_query_url]
            elif query_option == 2:
                self.start_urls = [standard_query_url]

        def set_rules(query_option):
            if query_option == 1:
                self.rules = (
                    # extended
                    Rule(
                        LinkExtractor(
                            allow=r"\?pg=\d+&view=grid&query=YToxOntzOjQ6InNvcnQiO3M6NjoiLXNjb3JlIjt9"
                        ),
                        callback='parse_page',
                        follow=True
                    ),
                    Rule(
                        LinkExtractor(
                            allow=r"objects/\d+"
                        ),
                        callback='parse_object',
                        follow=False
                    ),
                )
            elif query_option == 2:
                self.rules = (
                    # standard
                    Rule(
                        LinkExtractor(
                            allow=r"\?pg=\d+&view=grid&query=YTo2OntzOjg6Il9jdWx0dXJlIjtzOjU6IkdyZWVrIjtzOjIxOiIoY3VsdHVyZXxwbGFjZS5uYW1lcykiO3M6NToiR3JlZWsiO3M6MTM6ImRlcGFydG1lbnQuaWQiO2E6MTp7aTowO2k6Mzt9czoxNzoiX2ZpbHRlcl9oYXNfaW1hZ2UiO2I6MTtzOjk6Imhhc19pbWFnZSI7YjoxO3M6NDoic29ydCI7czo2OiItc2NvcmUiO30%3D"
                        ),
                        callback='parse_page',
                        follow=True
                    ),
                    Rule(
                        LinkExtractor(
                            allow=r"objects/\d+"
                        ),
                        callback='parse_object',
                        follow=False
                    ),
                )

        set_start_urls(self.option)
        set_rules(self.option)
        super(ArtSpider, self).__init__()

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, dont_filter=True)

    def parse_page(self, response):
        url = response.url
        page_number = url.split(page_url)[-1].split('&')[0]
        yield print_page_number(page_number)

    def parse_object(self, response):

        title =             response.xpath("//head//meta[@name='title']/@content").extract()
        date =              response.xpath("//head//meta[@name='date']/@content").extract()
        description =       response.xpath("//head//meta[@name='description']/@content").extract()
        keywords =          response.xpath("//head//meta[@name='keywords']/@content").extract()
        name =              response.xpath("//head//meta[@name='name']/@content").extract()
        culture =           response.xpath("//head//meta[@name='culture']/@content").extract()
        medium =            response.xpath("//head//meta[@name='medium']/@content").extract()
        dimensions =        response.xpath("//head//meta[@name='dimensions']/@content").extract()
        # workaround for maker --> simple parser; if multiple, maker is a list
        maker =             response.xpath('//p[@class="multiple makers"]/a[contains(@href, "artists")]/text()').extract()
        if isinstance(maker, list) and len(maker) != 0:
            unique_seperator = util.UNIQUE_SEPERATOR
            maker = [unique_seperator.join(maker)]
        else:
            maker = response.xpath("//head//meta[@name='maker']/@content").extract()
        classification =    response.xpath("//head//meta[@name='classification']/@content").extract()
        place_created =     response.xpath("//head//meta[@name='place_created']/@content").extract()
        object_type =       response.xpath("//head//meta[@name='object_type']/@content").extract()
        object_number =     response.xpath("//head//meta[@name='object_number']/@content").extract()
        image =             response.xpath("//head//meta[@name='image']/@content").extract()
        department =        response.xpath("//head//meta[@name='department']/@content").extract()

        item = [title, date, description, keywords, name, culture, medium, dimensions, maker, classification,
                place_created, object_type, object_number, image, department]

        title_encode =          str(item[0][0])  if len(item[0]) != 0 else str('')
        date_encode =           str(item[1][0])  if len(item[1]) != 0 else str('')
        description_encode =    str(item[2][0])  if len(item[2]) != 0 else str('')
        keywords_encode =       str(item[3][0])  if len(item[3]) != 0 else str('')
        name_encode =           str(item[4][0])  if len(item[4]) != 0 else str('')
        culture_encode =        str(item[5][0])  if len(item[5]) != 0 else str('')
        medium_encode =         str(item[6][0])  if len(item[6]) != 0 else str('')
        dimensions_encode =     str(item[7][0])  if len(item[7]) != 0 else str('')
        maker_encode =          str(item[8][0])  if len(item[8]) != 0 else str('')
        classification_encode = str(item[9][0])  if len(item[9]) != 0 else str('')
        place_created_encode =  str(item[10][0]) if len(item[10]) != 0 else str('')
        object_type_encode =    str(item[11][0]) if len(item[11]) != 0 else str('')
        object_number_encode =  str(item[12][0]) if len(item[12]) != 0 else str('')
        image_encode =          str(item[13][0]) if len(item[13]) != 0 else str('')
        department_encode =     str(item[14][0]) if len(item[14]) != 0 else str('')

        scraped_info = {
            'page' : response.url,
            'title' : title_encode,
            'date' : date_encode,
            'description' : description_encode,
            'keywords' : keywords_encode,
            'name' : name_encode,
            'culture' : culture_encode,
            'medium' : medium_encode,
            'dimensions' : dimensions_encode,
            'maker' : maker_encode,
            'classification' : classification_encode,
            'place_created' : place_created_encode,
            'object_type' : object_type_encode,
            'object_number' : object_number_encode,
            'image' : image_encode,
            'department' : department_encode
        }
        yield dump_json(scraped_info, self.option)
        # yield debug(scraped_info, "maker")


def run_crawler(option=2):
    if option == 1:
        if not os.path.exists(output_path + extended_path + "image_exists"):
            os.makedirs(output_path + extended_path + "image_exists")
        if not os.path.exists(output_path + extended_path + "no_image"):
            os.makedirs(output_path + extended_path + "no_image")
    elif option == 2:
        if not os.path.exists(output_path + standard_path):
            os.makedirs(output_path + standard_path)

    process = CrawlerProcess()
    process.crawl(ArtSpider, option)
    process.start()


if __name__ == '__main__':
    run_crawler()

