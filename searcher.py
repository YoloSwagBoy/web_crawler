# import the necessary packages
import numpy as np
import csv
import cv2
from scipy.spatial import distance as dist

class Searcher:
    def __init__(self, indexPath):
        self.indexPath = indexPath

    def search(self, id_list, queryFeatures, limit=5):
        results = {}


        with open(self.indexPath) as f:

            reader = csv.reader(f)

            for row in reader:
                string = row[0]
                if "/" in string:
                    id_string = string.replace(".jpg", "").split("/")[-1]
                else:
                    id_string = string.replace(".jpg", "").split("\\")[-1]

                id_string = int(id_string)
                if id_string in id_list:
                    features = [float(x) for x in row[1:]]
                    d = self.euclideanDistance(features, queryFeatures)
                    results[row[0]] = d

            f.close()


        results = sorted([(v, k) for (k, v) in results.items()])
        return results[:limit]

    def euclideanDistance(self, histA, histB, eps=1e-10):
        d = dist.euclidean(histA, histB)
        return d