import os
import json
import re

###########################
#                         #
#     simple patterns     #
#                         #
###########################

# all dates occurring in the library are handled
# introducing new patterns requires implementing new handlers for these patterns

#######################################################################################################################
#
# (about|early|late) Xth century (B.C.|A.D.) (?|or later)
#

pattern_Xth_century = \
    re.compile(r'(about +|early +|late +|after +)?[1-9][0-9]*(th|st|nd|rd) +century( +(B\.C\.|A\.D\.))$')


def xth_century_handler(date_string):
    reworked_string_list = []
    earliest, latest = -1, -1
    split_string = date_string.split(" ")
    if len(split_string) == 3:
        reworked_string_list = split_string
    elif len(split_string) == 4:
        reworked_string_list = [split_string[1]] + [split_string[2]] + [split_string[3]] + [split_string[0]]
    century = float(reworked_string_list[0].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""))
    # 4th century B.C. --> 400 B.C. - 301 B.C.
    # 4th century A.D. --> 301 A.D. - 400 A.D.
    if reworked_string_list[2] == 'B.C.':
        earliest = century * -100
        latest = century * -100 + 99
        if len(reworked_string_list) == 4:
            if reworked_string_list[3] == 'about':
                earliest -= 50
                latest += 51
            elif reworked_string_list[3] == 'late':
                earliest += 75
            elif reworked_string_list[3] == 'early':
                latest -= 74
            elif reworked_string_list[3] == 'after':  # latest date unknown --> inf
                earliest += 100
                latest = float('inf')
    elif reworked_string_list[2] == "A.D.":
        earliest = (century - 1) * 100 + 1
        latest = century * 100

    return [earliest, latest]

#######################################################################################################################
#
# (about|before) X (B.C.|A.D.)
#


pattern_X = re.compile(r'(about +|before +|after +)?[1-9][0-9]* +(B\.C\.|A\.D\.)?$')


def x_handler(date_string):
    reworked_string_list = date_string.split(" ")
    if len(reworked_string_list) == 3:
        reworked_string_list = [reworked_string_list[1], reworked_string_list[2], reworked_string_list[0]]

    earliest, latest = -1, -1
    if reworked_string_list[1] == "B.C.":
        earliest = latest = float(reworked_string_list[0])*-1
        if reworked_string_list[-1] == 'about':
            earliest -= 25
            latest += 25
        elif reworked_string_list[-1] == 'before':
            earliest = float('-inf')
        elif reworked_string_list[-1] == 'after':
            latest = float('inf')
    # no objects with this pattern and A.D.
    # elif reworked_string_list[1] == "A.D.":
    #    earliest = latest = int(reworked_string_list[0])

    return [earliest, latest]

#######################################################################################################################
#
# (about) X - Y (B.C.|A.D.)
#


pattern_XtoY = re.compile(r'(about +)?[1-9][0-9]* *- *[1-9][0-9]* +(B\.C\.|A\.D\.)$')


def x_to_y_handler(date_string):
    date_spaced = date_string
    if " - " not in date_spaced:
        date_spaced = date_spaced.replace("-", " - ")
    reworked_string_list = date_spaced.split(" ")
    # split_string is either 4 or 5 strings long --> (about) X - Y B.C.|A.D.
    if len(reworked_string_list) == 5:
        reworked_string_list = [reworked_string_list[1], reworked_string_list[3],
                                reworked_string_list[4], reworked_string_list[0]]
    else:
        reworked_string_list = [reworked_string_list[0], reworked_string_list[2], reworked_string_list[3]]

    earliest, latest = -1, -1
    if reworked_string_list[2] == "B.C.":
        earliest = float(reworked_string_list[0])*-1
        latest = float(reworked_string_list[1])*-1
    elif reworked_string_list[2] == "A.D.":
        earliest = float(reworked_string_list[0])
        latest = float(reworked_string_list[1])

    return [earliest, latest]


#######################################################################################################################
#
# (about) X (B.C.) - Y (B.C.)
#


pattern_XtoY_extended = re.compile(r'(about +)?[1-9][0-9]*( +B\.C\.) *- *[1-9][0-9]*( +B\.C\.)$')


def x_to_y_extended_handler(date_string):
    reworked_string_list = date_string.split(" ")
    reworked_string_list = [reworked_string_list[1], reworked_string_list[4]]
    earliest, latest = float(reworked_string_list[0])*-1, float(reworked_string_list[1])*-1
    return [earliest, latest]


#######################################################################################################################
#
# Xth - Yth century B.C.|A.D.
#


pattern_Xth_to_Yth_century = \
    re.compile(r'(late +)?[1-9][0-9]*(th|st|nd|rd) *- *(early +)?[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.)$')


def xth_to_yth_century_handler(date_string):
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")  # 5 to 7 --> (late) Xth - (early) Yth century (B.C.|A.D.)

    if len(reworked_string_list) == 5:
        reworked_string_list = \
            [reworked_string_list[0].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[2].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[4]]
    if len(reworked_string_list) == 6:
        reworked_string_list = \
            [reworked_string_list[1].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[3].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[5], reworked_string_list[0]]
    if len(reworked_string_list) == 7:
        reworked_string_list = \
            [reworked_string_list[1].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[4].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[6], reworked_string_list[0], reworked_string_list[3]]

    earliest, latest = -1, -1
    if reworked_string_list[2] == 'B.C.':
        earliest = float(reworked_string_list[0])*-100
        if reworked_string_list[-2] == 'late' or reworked_string_list[-1] == 'late':
            earliest += 75
        latest = float(reworked_string_list[1])*-100 + 99
        if reworked_string_list[-1] == 'early':
            latest -= 74
    elif reworked_string_list[2] == 'A.D.':
        earliest = float(reworked_string_list[0])*100 - 99
        latest = float(reworked_string_list[1])*100

    return [earliest, latest]


#######################################################################################################################
#
# half of century B.C.|A.D.
#


pattern_half_Xth_century = re.compile(r'(first|second) +half +of +[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.)$')


def half_xth_century_handler(date_string):
    reworked_string_list = date_string.split(" ")  # length is exactly 6
    reworked_string_list = \
        [reworked_string_list[3].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
         reworked_string_list[5], reworked_string_list[0]]

    earliest, latest = -1, -1
    if reworked_string_list[1] == 'B.C.':
        if reworked_string_list[-1] == 'first':
            earliest = float(reworked_string_list[0])*-100
            latest = earliest + 49
        elif reworked_string_list[-1] == 'second':
            earliest = float(reworked_string_list[0])*-100 + 50
            latest = earliest + 49
    elif reworked_string_list[1] == 'A.D.':
        if reworked_string_list[-1] == 'first':
            earliest = float(reworked_string_list[0])*100 - 99
            latest = earliest + 49
        elif reworked_string_list[-1] == 'second':
            earliest = float(reworked_string_list[0])*100 - 49
            latest = earliest + 49

    return [earliest, latest]


#######################################################################################################################
#
# quarter of century B.C.
#


pattern_quarter_Xth_century = \
    re.compile(r'(first|second|third|fourth) +quarter +of +[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.)$')


def quarter_xth_century_handler(date_string):
    reworked_string_list = date_string.split(" ")
    reworked_string_list = \
        [reworked_string_list[3].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
         reworked_string_list[0]]

    earliest, latest = float(reworked_string_list[0])*-100, -1
    if reworked_string_list[-1] == 'second':
        earliest += 25
    elif reworked_string_list[-1] == 'third':
        earliest += 50
    elif reworked_string_list[-1] == 'fourth':
        earliest += 75
    latest = earliest + 24
    return [earliest, latest]


#######################################################################################################################
#
# Xth century B.C.|A.D - Yth century B.C|A.D
#

pattern_Xth_century_to_Yth_century = \
    re.compile(r'(late +|about +)?[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.) *- *' +
               r'(early +)?[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.)$')


def xth_century_to_yth_century_handler(date_string):
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")  # length is 7 to 9, all cases B.C. to A.D.

    if len(reworked_string_list) == 7:
        reworked_string_list = \
            [reworked_string_list[0].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[4].replace("th", "").replace("st", "").replace("nd", "").replace("rd", "")]
    elif len(reworked_string_list) == 8:
        reworked_string_list = \
            [reworked_string_list[1].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[5].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[0]]
    elif len(reworked_string_list) == 9:
        reworked_string_list = \
            [reworked_string_list[1].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[6].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
             reworked_string_list[0], reworked_string_list[5]]

    earliest = float(reworked_string_list[0])*-100
    if reworked_string_list[-2] == 'late' or reworked_string_list[-1] == 'late':
        earliest += 75

    latest = float(reworked_string_list[1])*100
    if reworked_string_list[-1] == 'early':
        latest -= 75

    return [earliest, latest]


#######################################################################################################################
#
# Xth to half century
#


pattern_Xth_to_half_century = \
    re.compile(r'(late +)?[1-9][0-9]*(th|st|nd|rd) *- *' +
               r'(first|second) +half +of +[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.)$')


def xth_to_half_century_handler(date_string):
    # all strings are the same !
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")
    reworked_string_list = \
        [reworked_string_list[1].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
         reworked_string_list[6].replace("th", "").replace("st", "").replace("nd", "").replace("rd", "")]
    earliest = float(reworked_string_list[0])*-100 + 75
    latest = float(reworked_string_list[1])*-100 + 49
    return [earliest, latest]


#######################################################################################################################
#
# millennium
#


pattern_millennium = re.compile(r'[1-9][0-9]*(th|st|nd|rd) *- *[1-9][0-9]*(th|st|nd|rd) +millennium +B\.C\.$')


def millennium_handler(date_string):
    # all string are the same !
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")
    reworked_string_list = \
        [reworked_string_list[0].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""),
         reworked_string_list[2].replace("th", "").replace("st", "").replace("nd", "").replace("rd", "")]

    earliest = float(reworked_string_list[0])*-1000
    latest = earliest + 999

    return [earliest, latest]


#######################################################################################################################
#
# age
#


pattern_age = re.compile(r'(Ancient|Modern)$')


def age_handler(date_string):
    # according to wikipedia: 3000 BC – AD 500
    earliest, latest = -1, -1
    if date_string == "Ancient":
        earliest = -3000
        latest = 500
    # time span not defined clearly --> 1500 to present
    elif date_string == "Modern":
        earliest = 1500
        latest = 2020

    return [earliest, latest]


#######################################################################################################################
#
# unknown date
#


pattern_unknown = re.compile(r'(uncertain$|n\.d\.$|$)')


def unknown_handler():
    return [float('-inf'), float('inf')]


#######################################################################################################################
#
# X B.C.-A.D. Y
#


pattern_X_BC_AD_Y = re.compile(r'[1-9][0-9]* +B\.C\. *- *A\.D\. +[1-9][0-9]*$')


def x_bc_ad_y_handler(date_string):
    reworked_string_list = date_string.split(" ")
    earliest = float(reworked_string_list[0])*-1
    latest = float(reworked_string_list[-1])
    return [earliest, latest]


##################################
#                                #
#     very "unique" patterns     #
#                                #
##################################


#######################################################################################################################
#
# weird Xth century
#


pattern_Xth_century_weird = \
    re.compile(r'((early(-| +to +)|second +quarter-)?mid-(second +half +)?|post-|gem +|last +third +of +)?' +
               r'([1-9][0-9]*(th|st|nd|rd)|mid-fourth) +century( +B\.C\.| +A\.D\.)?' +
               r'( +or +(modern|Modern)| +or +later|-Modern|; +ring +modern)?( +\?)?$')


def xth_century_weird_handler(date_string):
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")
    if reworked_string_list[-1] == '?':  # remove question mark
        reworked_string_list = reworked_string_list[:-1]

    earliest, latest = -1, -1
    if reworked_string_list[0] == 'mid':
        if len(reworked_string_list) == 5:
            reworked_string_list = [reworked_string_list[2], reworked_string_list[-1]]
            if reworked_string_list[0] == 'fourth':
                reworked_string_list[0] = '4th'
            century = \
                float(reworked_string_list[0].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""))
            earliest = century*-100 + 25
            latest = earliest + 49
        elif len(reworked_string_list) == 7:  # 1 case --> mid-second half of
            reworked_string_list = [reworked_string_list[4], reworked_string_list[-1]]
            century = \
                float(reworked_string_list[0].replace("th", "").replace("st", "").replace("nd", "").replace("rd", ""))
            earliest = century*-100 + 25
            latest = earliest + 74
        return [earliest, latest]

    if len(reworked_string_list) == 2 and reworked_string_list[-1] == 'century':
        reworked_string_list += ['A.D.']
    if (reworked_string_list[-2], reworked_string_list[-1]) == ('-', 'Modern')\
            or (reworked_string_list[-2], reworked_string_list[-1]) == ('or', 'Modern')\
            or (reworked_string_list[-2], reworked_string_list[-1]) == ('or', 'modern'):
        reworked_string_list = reworked_string_list[:-2]

    reworked_string = " "
    date_type = ""
    if (reworked_string_list[0], reworked_string_list[1]) == ('post', '-'):
        reworked_string_list = reworked_string_list[2:]
    if len(reworked_string_list) == 3:
        reworked_string = reworked_string.join(reworked_string_list)
    elif len(reworked_string_list) == 5 and (reworked_string_list[-2], reworked_string_list[-1]) == ('or', 'later'):
        reworked_string = reworked_string.join(reworked_string_list[0:-2])
        date_type = 'or_later'
    elif len(reworked_string_list) == 7 \
            and (reworked_string_list[0], reworked_string_list[1], reworked_string_list[2]) == ('early', 'to', 'mid')\
            or (reworked_string_list[0], reworked_string_list[1], reworked_string_list[2]) == ('early', '-', 'mid'):
        reworked_string = reworked_string.join(reworked_string_list[4:])
        date_type = 'early_to_mid'
    elif len(reworked_string_list) == 8 and \
            (reworked_string_list[0], reworked_string_list[1], reworked_string_list[3]) == ('second', 'quarter', 'mid'):
        reworked_string = reworked_string.join(reworked_string_list[5:])
        date_type = 'quarter_mid'
    elif len(reworked_string_list) == 6 and\
            (reworked_string_list[0], reworked_string_list[1], reworked_string_list[2]) == ('last', 'third', 'of'):
        reworked_string = reworked_string.join(reworked_string_list[3:])
        date_type = 'last_third'
    elif len(reworked_string_list) == 6 and reworked_string_list[0] == 'gem':
        reworked_string_list = [reworked_string_list[1], reworked_string_list[2], 'B.C.']
        reworked_string = reworked_string.join(reworked_string_list)

    if pattern_Xth_century.match(reworked_string):
        if date_type == 'or_later':
            earliest, latest = xth_century_handler(reworked_string)
            latest = float('inf')
        elif date_type == 'early_to_mid':
            earliest, latest = xth_century_handler(reworked_string)
            latest -= 25
        elif date_type == 'quarter_mid':
            earliest, latest = xth_century_handler(reworked_string)
            earliest += 25
            latest -= 25
        elif date_type == 'last_third':
            earliest, latest = xth_century_handler(reworked_string)
            earliest += 67
        else:
            return xth_century_handler(reworked_string)

    return [earliest, latest]


#######################################################################################################################
#
# weird X B.C.|A.D.
#


pattern_X_weird = re.compile(r'(about +)?[1-9][0-9]* +B\.C\.( +or +modern( +forgery)?)?$')


def x_weird_handler(date_string):
    reworked_string_list = date_string.split(" ")
    year = float(reworked_string_list[1])*-1
    return [year - 25, year + 25]


#######################################################################################################################
#
# weird X - Y B.C.|A.D.
#


pattern_XtoY_weird = \
    re.compile(r'(about +)?[1-9][0-9]*(([/\-])[1-9][0-9]*)?( +B\.C\.)?' +
               r'( *- *| +or +)[1-9][0-9]*(([/\-])[1-9][0-9]*)?( +(B\.C\.|B\.C|BC))?$')


def x_to_y_weird_handler(date_string):
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    if "/" in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("/", " ")
        reworked_string_list = reworked_string_list.split(" ")
        reworked_string_list = \
            [reworked_string_list[0], reworked_string_list[2], reworked_string_list[4], reworked_string_list[5]]
    else:
        reworked_string_list = reworked_string_list.split(" ")

    if reworked_string_list[0] == 'about':
        reworked_string_list = reworked_string_list[1:]

    if "or" in reworked_string_list:
        if len(reworked_string_list) == 8:
            reworked_string_list = \
                [reworked_string_list[0], reworked_string_list[1], reworked_string_list[6], reworked_string_list[7]]
        elif len(reworked_string_list) == 6:
            reworked_string_list = \
                [reworked_string_list[0], reworked_string_list[1], reworked_string_list[4], reworked_string_list[5]]

    if len(reworked_string_list) == 3:
        reworked_string_list += ['A.D.']
    if reworked_string_list[-1] == 'BC':
        reworked_string_list[-1] = 'B.C.'
    if len(reworked_string_list) == 5 and reworked_string_list[1] == 'B.C.' and reworked_string_list[-1] == 'B.C':
        reworked_string_list = [reworked_string_list[0], reworked_string_list[2], reworked_string_list[3], 'B.C.']

    reworked_string = " "
    reworked_string = reworked_string.join(reworked_string_list)

    if pattern_XtoY.match(reworked_string):
        return x_to_y_handler(reworked_string)

    return [-1, -1]


#######################################################################################################################
#
# weird Xth - Yth century B.C.|A.D.
#

pattern_Xth_to_Yth_century_weird = \
    re.compile(r'(second +half +of +|late +|about +late +)?[1-9][0-9]*(th|st|nd|rd)( +or +| *- *)(early +)?' +
               r'[1-9][0-9]*(th|st|nd|rd) +century +B\.C\.$')


def x_th_to_y_th_century_weird_handler(date_string):
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")

    if reworked_string_list[0] == "about":
        reworked_string_list = reworked_string_list[1:]

    if reworked_string_list[1] == "or":
        reworked_string_list[1] = "-"
    if reworked_string_list[2] == "or":
        reworked_string_list[2] = "-"

    reworked_string = " "
    reworked_string = reworked_string.join(reworked_string_list)
    if pattern_Xth_to_Yth_century.match(reworked_string):
        return xth_to_yth_century_handler(reworked_string)
    else:
        reworked_string = " "
        reworked_string = reworked_string.join(reworked_string_list[:4] + reworked_string_list[-2:])
        earliest, latest = -1, -1
        if pattern_half_Xth_century.match(reworked_string):
            earliest, latest = half_xth_century_handler(reworked_string)
        century_string = reworked_string_list[5].replace("th", "").replace("st", "").replace("nd", "").replace("rd", "")
        latest_century = float(century_string)*-100 + 99
        return [earliest, latest_century]


#######################################################################################################################
#
# weird Xth century B.C.|A.D. - Yth century B.C.|A.D.
#


pattern_Xth_century_to_Yth_century_weird = \
    re.compile(r'((second +half|end) +of +|perhaps +)?[1-9][0-9]*(th|st|nd|rd) +century( +B\.C\.)? *- *' +
               r'(beginning +(of +)?)?[1-9][0-9]*(th|st|nd|rd) +century +(B\.C\.|A\.D\.)( +\?)?$')


def x_th_century_to_y_th_century_weird_handler(date_string):
    reworked_string_list = date_string
    if " - " not in reworked_string_list:
        reworked_string_list = reworked_string_list.replace("-", " - ")
    reworked_string_list = reworked_string_list.split(" ")

    if reworked_string_list[-1] == "?":
        reworked_string_list = reworked_string_list[:-1]
    if (reworked_string_list[0], reworked_string_list[1]) == ("end", "of"):
        reworked_string_list = ["late"] + reworked_string_list[2:]
    if reworked_string_list[0] == "perhaps":
        reworked_string_list[0] = "about"

    if (reworked_string_list[5], reworked_string_list[6]) == ("beginning", "of"):
        reworked_string_list = reworked_string_list[:5] + ["early"] + reworked_string_list[7:]

    reworked_string = " "
    reworked_string = reworked_string.join(reworked_string_list)
    if pattern_Xth_century_to_Yth_century.match(reworked_string):
        return xth_century_to_yth_century_handler(reworked_string)
    elif len(reworked_string_list) == 10 and\
            (reworked_string_list[0], reworked_string_list[1], reworked_string_list[2]) == ("second", "half", "of") and\
            reworked_string_list[6] == "beginning":
        early_century_string = \
            reworked_string_list[3].replace("th", "").replace("st", "").replace("nd", "").replace("rd", "")
        late_century_string = \
            reworked_string_list[7].replace("th", "").replace("st", "").replace("nd", "").replace("rd", "")
        earliest = float(early_century_string)*-100 + 50
        latest = float(late_century_string)*-100 + 25
        return [earliest, latest]


#
# end of patterns
#
#######################################################################################################################


#######################################################################################################################
#
# handle dates in all files
# only executed when this file is main
# prints all dates and calculates distribution, which formats were how often used
#


def handle_all_dates():
    # 13 buckets for main patterns, weird patterns in one bucket
    distribution_normal = [0]*13
    distribution_weird = 0

    # for formating and padding
    string_length = 60
    space = ' '

    path = 'output/standard/'
    file_counter = 0
    for filename in os.listdir(path):
        file_counter += 1
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)
            date = data['date'].replace(u"\u2013", "-")  # replace en dash
            match = ""
            if pattern_Xth_century.match(date):
                match = "MATCH 1"
                distribution_normal[0] += 1
            elif pattern_XtoY.match(date):
                match = "MATCH 2"
                distribution_normal[1] += 1
            elif pattern_X.match(date):
                match = "MATCH 3"
                distribution_normal[2] += 1
            elif pattern_Xth_to_Yth_century.match(date):
                match = "MATCH 4"
                distribution_normal[3] += 1
            elif pattern_half_Xth_century.match(date):
                match = "MATCH 5"
                distribution_normal[4] += 1
            elif pattern_Xth_century_to_Yth_century.match(date):
                match = "MATCH 6"
                distribution_normal[5] += 1
            elif pattern_XtoY_extended.match(date):
                match = "MATCH 7"
                distribution_normal[6] += 1
            elif pattern_quarter_Xth_century.match(date):
                match = "MATCH 8"
                distribution_normal[7] += 1
            elif pattern_Xth_to_half_century.match(date):
                match = "MATCH 9"
                distribution_normal[8] += 1
            elif pattern_unknown.match(date):
                match = "MATCH 10"
                distribution_normal[9] += 1
            elif pattern_millennium.match(date):
                match = "MATCH 11"
                distribution_normal[10] += 1
            elif pattern_X_BC_AD_Y.match(date):
                match = "MATCH 12"
                distribution_normal[11] += 1
            elif pattern_age.match(date):
                match = "MATCH 13"
                distribution_normal[12] += 1
            elif pattern_Xth_century_weird.match(date):
                match = "MATCH 14"
                distribution_weird += 1
            elif pattern_X_weird.match(date):
                match = "MATCH 15"
                distribution_weird += 1
            elif pattern_XtoY_weird.match(date):
                match = "MATCH 16"
                distribution_weird += 1
            elif pattern_Xth_to_Yth_century_weird.match(date):
                match = "MATCH 17"
                distribution_weird += 1
            elif pattern_Xth_century_to_Yth_century_weird.match(date):
                match = "MATCH 18"
                distribution_weird += 1

            earliest_date, latest_date = "ERROR", "ERROR"
            if match is "MATCH 1":
                earliest_date, latest_date = xth_century_handler(date)
            if match is "MATCH 2":
                earliest_date, latest_date = x_to_y_handler(date)
            if match is "MATCH 3":
                earliest_date, latest_date = x_handler(date)
            if match is "MATCH 4":
                earliest_date, latest_date = xth_to_yth_century_handler(date)
            if match is "MATCH 5":
                earliest_date, latest_date = half_xth_century_handler(date)
            if match is "MATCH 6":
                earliest_date, latest_date = xth_century_to_yth_century_handler(date)
            if match is "MATCH 7":
                earliest_date, latest_date = x_to_y_extended_handler(date)
            if match is "MATCH 8":
                earliest_date, latest_date = quarter_xth_century_handler(date)
            if match is "MATCH 9":
                earliest_date, latest_date = xth_to_half_century_handler(date)
            if match is "MATCH 10":
                earliest_date, latest_date = unknown_handler()
            if match is "MATCH 11":
                earliest_date, latest_date = millennium_handler(date)
            if match is "MATCH 12":
                earliest_date, latest_date = x_bc_ad_y_handler(date)
            if match is "MATCH 13":
                earliest_date, latest_date = age_handler(date)
            if match is "MATCH 14":
                earliest_date, latest_date = xth_century_weird_handler(date)
            if match is "MATCH 15":
                earliest_date, latest_date = x_weird_handler(date)
            if match is "MATCH 16":
                earliest_date, latest_date = x_to_y_weird_handler(date)
            if match is "MATCH 17":
                earliest_date, latest_date = x_th_to_y_th_century_weird_handler(date)
            if match is "MATCH 18":
                earliest_date, latest_date = x_th_century_to_y_th_century_weird_handler(date)

            assert earliest_date is not "ERROR" and latest_date is not "ERROR"

            padding_date_string = (string_length-len(date))*space
            padding_filename = (20 - len(filename))*space
            padding_date_earliest = (7 - len(str(earliest_date)))*space
            padding_to = space
            print(date + padding_date_string + filename + padding_filename +
                  str(earliest_date) + padding_date_earliest + " to " + padding_to + str(latest_date) + "\t")

    print()
    print(distribution_weird)
    print(distribution_normal)
    print("parsed " + str(sum(distribution_normal) + distribution_weird) + " out of " + str(file_counter) + " files")


#######################################################################################################################
#
# main handling function, use this to build data base
#


def parse_date_string(date_string):
    date_string = date_string.replace(u"\u2013", "-")
    earliest_date, latest_date = -1, -1
    if pattern_Xth_century.match(date_string):
        earliest_date, latest_date = xth_century_handler(date_string)
    elif pattern_XtoY.match(date_string):
        earliest_date, latest_date = x_to_y_handler(date_string)
    elif pattern_X.match(date_string):
        earliest_date, latest_date = x_handler(date_string)
    elif pattern_Xth_to_Yth_century.match(date_string):
        earliest_date, latest_date = xth_to_yth_century_handler(date_string)
    elif pattern_half_Xth_century.match(date_string):
        earliest_date, latest_date = half_xth_century_handler(date_string)
    elif pattern_Xth_century_to_Yth_century.match(date_string):
        earliest_date, latest_date = xth_century_to_yth_century_handler(date_string)
    elif pattern_XtoY_extended.match(date_string):
        earliest_date, latest_date = x_to_y_extended_handler(date_string)
    elif pattern_quarter_Xth_century.match(date_string):
        earliest_date, latest_date = quarter_xth_century_handler(date_string)
    elif pattern_Xth_to_half_century.match(date_string):
        earliest_date, latest_date = xth_to_half_century_handler(date_string)
    elif pattern_unknown.match(date_string):
        earliest_date, latest_date = unknown_handler()
    elif pattern_millennium.match(date_string):
        earliest_date, latest_date = millennium_handler(date_string)
    elif pattern_X_BC_AD_Y.match(date_string):
        earliest_date, latest_date = x_bc_ad_y_handler(date_string)
    elif pattern_age.match(date_string):
        earliest_date, latest_date = age_handler(date_string)
    elif pattern_Xth_century_weird.match(date_string):
        earliest_date, latest_date = xth_century_weird_handler(date_string)
    elif pattern_X_weird.match(date_string):
        earliest_date, latest_date = x_weird_handler(date_string)
    elif pattern_XtoY_weird.match(date_string):
        earliest_date, latest_date = x_to_y_weird_handler(date_string)
    elif pattern_Xth_to_Yth_century_weird.match(date_string):
        earliest_date, latest_date = x_th_to_y_th_century_weird_handler(date_string)
    elif pattern_Xth_century_to_Yth_century_weird.match(date_string):
        earliest_date, latest_date = x_th_century_to_y_th_century_weird_handler(date_string)

    return [earliest_date, latest_date]


if __name__ == "__main__":
    handle_all_dates()
