import webbrowser

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject, pyqtSlot, QModelIndex
from PyQt5.QtGui import QPixmap

from interface import Ui_Form
import search
import sys
from model import Model
from tinydb import TinyDB, Query
from PyQt5.QtWidgets import QCompleter, QAbstractItemView
import slider

class MainWindowUIClass(Ui_Form):
    def __init__(self):
        super().__init__()
        self.model = Model()
        self.imageList = []

    def setupUi(self, MW):
        super().setupUi(MW)
        self.imageList.append(self.image1)
        self.imageList.append(self.image2)
        self.imageList.append(self.image3)
        self.imageList.append(self.image4)
        self.imageList.append(self.image5)
        self.tableWidget.setColumnHidden(6, True)
        self.tableWidget.setEditTriggers(QtWidgets.QTableWidget.NoEditTriggers)
        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.earliest_date = None
        self.latest_date = None
        self.initInsertAll()
        # List of keywords for auto completion
        self.model.object_type_list = list(self.model.object_type_list)
        self.model.maker_list = list(self.model.maker_list)
        self.model.culture_list = list(self.model.culture_list)

        self.completer_maker = QCompleter(self.model.maker_list, self.lineEdit_Maker)
        self.completer_maker.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.lineEdit_Maker.setCompleter(self.completer_maker)
        self.completer_object_type = QCompleter(self.model.object_type_list, self.lineEdit_ObjectType)
        self.completer_object_type.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.lineEdit_ObjectType.setCompleter(self.completer_object_type)
        self.completer_culture = QCompleter(self.model.culture_list, self.lineEdit_Culture)
        self.completer_culture.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.lineEdit_Culture.setCompleter(self.completer_culture)

        self.slider = slider.QRangeSlider(MW)
        self.slider.setGeometry(20, 850, 600, 30)
        self.slider.setMin(self.earliest_date)
        self.slider.setMax(self.latest_date)
        self.slider.setStart(self.earliest_date)
        self.slider.setEnd(self.latest_date)


    def returnedPressedSlot(self):
        self.testButton()

    def performSearchAndUpdateBoxes(self, queryName):
        self.model.setImagesSet()
        id_list = self.model.getCurrentIdList()
        results = search.perform_search(id_list, queryName, "image_descriptors")
        self.model.setFoundImageList(results)
        results = self.model.getFoundImageList()
        for i in range(0, len(results)):
            tempPixmap = QPixmap(results[i][1])
            self.imageList[i].setPixmap(tempPixmap)
            self.imageList[i].resize(self.imageList[i].width(), self.imageList[i].height())

    def writeDoc(self):
        queryName = self.model.getQueryFileName()

        if(queryName != ""):
            self.performSearchAndUpdateBoxes(queryName)

    def browseSlot(self):
        # self.debugPrint( "Browse button pressed" )
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(
            None,
            "QFileDialog.getOpenFileName()",
            "",
            "All Files (*);;Python Files (*.py)",
            options=options)
        self.setQueryImage(fileName)

    def setQueryImage(self, filePath):
        pixmap = QPixmap(filePath)
        self.queryImage.setPixmap(pixmap)
        self.queryImage.resize(self.queryImage.width(), self.queryImage.height())
        self.model.setQueryFileName(filePath)

    def deleteAllRows(self):
        self.tableWidget.setRowCount(0)

    def fillTableWithData(self, data):
        id_list = []
        for i in data:
            self.tableWidget.insertRow(0)
            title = i["title"]

            date_early = int(i["date"][0]) if i["date"][0] > float('-inf') else -6000
            if date_early < 0:
                date_early = str(date_early*-1) + " B.C."
            else:
                date_early = str(date_early) + " A.D."
            date_late = int(i["date"][1]) if i["date"][1] < float('inf') else 2020
            if date_late < 0:
                date_late = str(date_late*-1) + " B.C."
            else:
                date_late = str(date_late) + " A.D."

            Date = date_early + " - " + date_late
            Object_type = "; ".join(i["object_type"])
            Place_created = i["place_created"]
            Culture = "; ".join(i["culture"])
            Maker = "; ".join(i["maker"])
            Id = str(i["id"])
            self.tableWidget.setItem(0, 0, QtWidgets.QTableWidgetItem(title))
            self.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(Date))
            self.tableWidget.setItem(0, 2, QtWidgets.QTableWidgetItem(Object_type))
            self.tableWidget.setItem(0, 3, QtWidgets.QTableWidgetItem(Place_created))
            self.tableWidget.setItem(0, 4, QtWidgets.QTableWidgetItem(Culture))
            self.tableWidget.setItem(0, 5, QtWidgets.QTableWidgetItem(Maker))
            self.tableWidget.setItem(0, 6, QtWidgets.QTableWidgetItem(Id))
            id_list.append(i["id"])
        self.model.setCurrentIdList(id_list)

    def testButton( self ):
        title = self.lineEdit_Title.text()
        maker = self.lineEdit_Maker.text()
        culture = self.lineEdit_Culture.text()
        object_type = self.lineEdit_ObjectType.text()
        date_earliest = self.slider.start()
        date_latest = self.slider.end()
        #if title != "" or maker != "" or culture != "" or object_type != "":
        data = self.model.getQueryData(title_string=title, maker_string=maker, object_type_string=object_type,
                                           culture_string=culture, date_earliest=date_earliest,
                                           date_latest=date_latest)
        #else:
        #    data = self.model.getAllData()
        self.deleteAllRows()
        self.fillTableWithData(data)


    def initInsertAll(self):
        id_list = []
        data = self.model.getAllData()
        min = float('inf')
        max = float('-inf')
        for i in data:
            self.tableWidget.insertRow(0)
            title = i["title"]

            date_early = int(i["date"][0]) if i["date"][0] > float('-inf') else -6000
            if date_early < 0:
                date_early = str(date_early*-1) + " B.C."
            else:
                date_early = str(date_early) + " A.D."
            date_late = int(i["date"][1]) if i["date"][1] < float('inf') else 2020
            if date_late < 0:
                date_late = str(date_late*-1) + " B.C."
            else:
                date_late = str(date_late) + " A.D."

            Date = date_early + " - " + date_late
            Object_type = "; ".join(i["object_type"])
            Place_created = i["place_created"]
            Culture = "; ".join(i["culture"])
            Maker = "; ".join(i["maker"])
            Id = str(i["id"])
            self.tableWidget.setItem(0, 0, QtWidgets.QTableWidgetItem(title))
            self.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem(Date))
            self.tableWidget.setItem(0, 2, QtWidgets.QTableWidgetItem(Object_type))
            self.tableWidget.setItem(0, 3, QtWidgets.QTableWidgetItem(Place_created))
            self.tableWidget.setItem(0, 4, QtWidgets.QTableWidgetItem(Culture))
            self.tableWidget.setItem(0, 5, QtWidgets.QTableWidgetItem(Maker))
            self.tableWidget.setItem(0, 6, QtWidgets.QTableWidgetItem(Id))
            id_list.append(i["id"])

            for object_type in i["object_type"]:
                self.model.object_type_list.add(object_type)
            for maker in i["maker"]:
                self.model.maker_list.add(maker)
            for culture in i["culture"]:
                self.model.culture_list.add(culture)

            if i["date"][0] < min and i["date"][0] > float('-inf'):
                min = i["date"][0]
            if i["date"][1] > max and i["date"][1] < float('inf'):
                max = i["date"][1]

        # boundaries for slider date
        self.earliest_date = int(min)
        self.latest_date = int(max)
        self.model.setCurrentIdList(id_list)

        #self.tableWidget.sortItems(0, QtCore.Qt.AscendingOrder)

    def itemClicked(self, row, column):
        id = self.tableWidget.item(row, 6).text()
        filepath = "images/"+id+".jpg"
        intID = int(id)
        self.setSelectedItem(filepath, intID)

    def cellDoubleClicked(self, row, column):
        id = self.tableWidget.item(row, 6).text()
        link = self.model.getLinkById(int(id))
        webbrowser.open(link)

    def image1Clicked( self ):
        if(self.model.isImagesSet()):
            path = self.model.foundImageList[0][1]
            link,id = self.parseImageName(path)
            self.setSelectedItem(path,id)
            webbrowser.open(link)

    def image2Clicked( self ):
        if (self.model.isImagesSet()):
            path = self.model.foundImageList[1][1]
            link, id = self.parseImageName(path)
            self.setSelectedItem(path, id)
            webbrowser.open(link)

    def image3Clicked( self ):
        if (self.model.isImagesSet()):
            path = self.model.foundImageList[2][1]
            link, id = self.parseImageName(path)
            self.setSelectedItem(path, id)
            webbrowser.open(link)

    def image4Clicked( self ):
        if (self.model.isImagesSet()):
            path = self.model.foundImageList[3][1]
            link, id = self.parseImageName(path)
            self.setSelectedItem(path, id)
            webbrowser.open(link)

    def image5Clicked( self ):
        if (self.model.isImagesSet()):
            path = self.model.foundImageList[4][1]
            link, id = self.parseImageName(path)
            self.setSelectedItem(path, id)
            webbrowser.open(link)

    def parseImageName(self, string):
        ind_id = 0
        if(string == ""):
            return -1
        else:
            print(string)
            if "/" in string:
                id_string = string.replace(".jpg", "").split("/")[-1]
            else:
                id_string = string.replace(".jpg", "").split("\\")[-1]
            print(id_string)
            int_id = int(id_string)
            link = self.model.getLinkById(int_id)
            return link, int_id

    def setSelectedItem(self, path, id):
        self.model.setFindSimilarFileName(path)
        self.selectedItemInfo.clear()
        pixmap = QPixmap(path)
        self.selectedItem.setPixmap(pixmap)
        self.selectedItem.resize(self.selectedItem.width(), self.selectedItem.height())
        data = self.model.getDataById(id)
        title = data["title"]
        date_early = int(data["date"][0]) if data["date"][0] > float('-inf') else -6000
        date_early = str(date_early*-1) + " B.C." if date_early < 0 else str(date_early) + " A.D."
        date_late = int(data["date"][1]) if data["date"][1] < float('inf') else 2020
        date_late = str(date_late*-1) + " B.C." if date_late < 0 else str(date_late) + " A.D."
        date = date_early + " - " + date_late
        dimensions = data["dimensions"]
        object_type = "; ".join(data["object_type"])
        place_created = data["place_created"]
        culture = "; ".join(data["culture"])
        maker = "; ".join(data["maker"])
        medium = data["medium"]

        self.selectedItemInfo.append("<html><b>title:</b></html> " + title)
        self.selectedItemInfo.append("<html><b>date:</b></html> " + date)
        self.selectedItemInfo.append("<html><b>dimensions:</b></html> " + dimensions)
        self.selectedItemInfo.append("<html><b>culture:</b></html> " + culture)
        self.selectedItemInfo.append("<html><b>object type:</b></html> " + object_type)
        self.selectedItemInfo.append("<html><b>medium:</b></html> " + medium)
        self.selectedItemInfo.append("<html><b>place created:</b></html> " + place_created)
        self.selectedItemInfo.append("<html><b>maker:</b></html> " + maker)

    def sortTableChanged( self, index ):
        if(index == 0):
            self.tableWidget.sortItems(0, QtCore.Qt.AscendingOrder)
        elif(index == 1):
            self.tableWidget.sortItems(0, QtCore.Qt.DescendingOrder)
        elif (index == 2):
            self.tableWidget.sortItems(3, QtCore.Qt.AscendingOrder)
        elif (index == 3):
            self.tableWidget.sortItems(3, QtCore.Qt.DescendingOrder)
        elif (index == 4):
            self.tableWidget.sortItems(2, QtCore.Qt.AscendingOrder)
        elif (index == 5):
            self.tableWidget.sortItems(2, QtCore.Qt.DescendingOrder)

    def findSimilar( self ):
        queryName = self.model.getFindSimilarFileName()
        if (queryName != ""):
            self.performSearchAndUpdateBoxes(queryName)

def main():
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = MainWindowUIClass()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

main()