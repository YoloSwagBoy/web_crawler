# import the necessary packages
from descriptor import ColorDescriptor
import argparse
import glob
import cv2

def index_images(bins):
    cd = ColorDescriptor(bins)

    output = open("image_descriptors", "w")
    for imagePath in glob.glob("images/" + "/*.jpg"):
        imageID = imagePath[imagePath.rfind("/") + 1:]
        image = cv2.imread(imagePath)

        features = cd.describe(image)

        features = [str(f) for f in features]
        output.write("%s,%s\n" % (imageID, ",".join(features)))

    output.close()

if __name__ == "__main__":
    index_images((3, 18, 3))