import scrapy
from scrapy.crawler import CrawlerProcess
import scrapy.settings.default_settings as SETTINGS
SETTINGS.LOG_LEVEL = 'INFO'
SETTINGS.COOKIES_ENABLED = False


class TestScraper(scrapy.Spider):
    name = "getty.edu"
    start_urls = [
        "http://www.getty.edu/art/collection/objects/11731/"
    ]

    def parse(self, response):
        title = response.xpath('//p[@class="multiple makers"]/a[contains(@href, "artists")]/text()').extract()
        if isinstance(title, list):
            unique_seperator = '<UNIQUE_SEPERATOR>'
            title = [unique_seperator.join(title)]
        yield print(title)


def run_crawler():

    process = CrawlerProcess()
    process.crawl(TestScraper)
    process.start()


if __name__ == "__main__":
    run_crawler()
