# import the necessary packages
from descriptor import ColorDescriptor
from searcher import Searcher
import argparse
import cv2


def perform_search( id_list, query_image, image_descriptors):
    cd = ColorDescriptor((3, 18, 3))


    query = cv2.imread(query_image)
    features = cd.describe(query)


    searcher = Searcher(image_descriptors)
    results = searcher.search(id_list, features)

    return results

if __name__ == "__main__":
    results = perform_search("testimage.jpg", "image_descriptors")
    for (score, resultID) in results:
        print("The picture: " + str(resultID) + "with the sim: " + str(score))