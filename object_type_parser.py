import os
import json

path = 'output/standard/'

dummy_dict = {}


def parse_object_type(string):
    dummy_list = string.split(" / ")
    return dummy_list


def handle_all_object_types():
    for filename in os.listdir(path):
        with open(path + filename, encoding='utf-8') as json_file:
            data = json.load(json_file)

            key_string = data["object_type"] if len(data["object_type"]) != 0 else "UNKNOWN"
            key_string = key_string.replace("(?)", "")
            key_string = key_string.replace(" and ", " / ")
            if "figure" in key_string or "Figure" in key_string:
                key_string = "Figure"
            elif "cup" in key_string or "Cup" in key_string:
                key_string = "Cup"
            elif "Jug" in key_string:
                key_string = "Jug"
            elif "Jewelry" in key_string:
                key_string = "Jewelry"
            elif "Vase" in key_string or "vase" in key_string:
                key_string = "Vase"
            elif "Calyx" in key_string:
                key_string = "Calyx"
            elif "Seal" in key_string:
                key_string = "Seal"
            elif "Vessel" in key_string or "vessel" in key_string:
                key_string = "Vessel"
            elif "Krater" in key_string:
                key_string = "Krater"


            object_type_list = parse_object_type(key_string)

            # print(object_type_list)
            # print("\t\t" + filename)

            for object_type in object_type_list:
                if object_type not in dummy_dict:
                    dummy_dict[object_type] = 1
                else:
                    dummy_dict[object_type] += 1

    for x in sorted(dummy_dict):
        print(x + (50 - len(x))*" " + str(dummy_dict[x]))


def parse_object_type_string(object_type_string):
    key_string = object_type_string if len(object_type_string) != 0 else "UNKNOWN"
    key_string = key_string.replace("(?)", "")
    key_string = key_string.replace(" and ", " / ")
    if "figure" in key_string or "Figure" in key_string:
        key_string = "Figure"
    elif "cup" in key_string or "Cup" in key_string:
        key_string = "Cup"
    elif "Jug" in key_string:
        key_string = "Jug"
    elif "Jewelry" in key_string:
        key_string = "Jewelry"
    elif "Vase" in key_string or "vase" in key_string:
        key_string = "Vase"
    elif "Calyx" in key_string:
        key_string = "Calyx"
    elif "Seal" in key_string:
        key_string = "Seal"
    elif "Vessel" in key_string or "vessel" in key_string:
        key_string = "Vessel"
    elif "Krater" in key_string:
        key_string = "Krater"

    return parse_object_type(key_string)


if __name__ == "__main__":
    handle_all_object_types()

# TODO: make dict with object_type as key and filename as value --> faster searching, probably
