from tinydb import TinyDB, Query
import re
class Model:
    def __init__(self):
        self.queryFileName = ""
        self.foundImageList = []
        self.dataBase = TinyDB("database/db.json")
        self.maker_list = set()
        self.object_type_list = set()
        self.culture_list = set()
        self.findSimilarFileName = ""
        self.imagesSet = False
        self.currentIdList = []

    def setCurrentIdList(self, idList):
        self.currentIdList = idList

    def getCurrentIdList(self):
        return self.currentIdList

    def setImagesSet(self):
        self.imagesSet = True

    def isImagesSet(self):
        return self.imagesSet

    def isValid(self, queryFileName):
        try:
            file = open(queryFileName, 'r')
            file.close()
            return True
        except:
            return False

    def setFindSimilarFileName(self, similarFileName):
        if self.isValid(similarFileName):
            self.findSimilarFileName = similarFileName
        else:
            self.findSimilarFileName = ""

    def getFindSimilarFileName(self):
        return self.findSimilarFileName

    def setQueryFileName(self, queryFileName):
        if self.isValid(queryFileName):
            self.queryFileName = queryFileName
        else:
            self.queryFileName = ""

    def getQueryFileName(self):
        return self.queryFileName

    def getFoundImageList(self):
        return self.foundImageList

    def setFoundImageList(self, Foundlist):
        self.foundImageList = Foundlist

    def getAllData(self):
        data = self.dataBase.all()
        return data

    def getQueryData(self, title_string="", maker_string="", culture_string="", object_type_string="",
                     date_earliest=-6000, date_latest=2020):
        query = Query()
        queries = None
        if title_string != "":
            queries = query.title.search(title_string, flags=re.IGNORECASE)
        if maker_string != "":
            if queries is None:
                queries = query.maker.any(maker_string)
            else:
                queries &= query.maker.any(maker_string)
        if culture_string != "":
            if queries is None:
                queries = query.culture.any(culture_string)
            else:
                queries &= query.culture.any(culture_string)
        if object_type_string != "":
            if queries is None:
                queries = query.object_type.any(object_type_string)
            else:
                queries &= query.object_type.any(object_type_string)

        if queries is None:
            queries = query.date[0] <= date_latest
            queries &= query.date[1] >= date_earliest
        else:
            queries &= query.date[0] <= date_latest
            queries &= query.date[1] >= date_earliest

        data = self.dataBase.search(queries)
        return data

    def getLinkById(self, id):
        id_query = Query()
        link = self.dataBase.search(id_query.id == id)[0]
        ret = link['page']
        return ret

    def getDataById(self, id):
        id_query = Query()
        data = self.dataBase.search(id_query.id == id)[0]
        return data